def dijkstra_algorithm_simple(graph,src):
    if src not in graph.nodes:
        raise AttributeError
    vertices = {}
    results = {}
    previous = {}
    #Setting initial values for the vertices
    for node in graph.nodes:
        if node == src:
            vertices[node] = 0
        else:
            vertices[node] = float("inf")
    #Main loop - iterates over vertices and checks if there is shorter way
    while len(vertices)>0:
        queue = sorted(vertices,key=vertices.get)[0]
        node = queue[0]
        for neighbor in graph.neighbors(node):
            #That's needed because graph doesn't know anything about vertices that has been processed
            if neighbor in results.keys():
                continue
            if (vertices[node]+graph.get_edge_data(node,neighbor)['weight']) < vertices[neighbor]:
                vertices[neighbor] = round(vertices[node]+graph.get_edge_data(node,neighbor)['weight'],2)
                previous[neighbor] = node
        results[node] = vertices[node]
        del vertices[node]
    return results, previous
