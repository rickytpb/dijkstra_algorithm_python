This application was written in Python 2.7
Required libraries are listed in requirements.txt - just run "pip install -r requirements.txt".

It's a simple implementation of dijkstra algorithm using networkx library for graph representation.
Run main.py and select a node that you'd like to treat as a source - it will show a visual representation of a graph.
