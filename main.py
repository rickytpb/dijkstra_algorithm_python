import networkx as nx
import matplotlib.pyplot as plt
import sys
from dijkstra import dijkstra_algorithm_simple

def compose_graph():
    G = nx.Graph()
    G.add_edge('a', 'b', weight=0.6)
    G.add_edge('a', 'c', weight=0.2)
    G.add_edge('c', 'd', weight=0.1)
    G.add_edge('c', 'e', weight=0.7)
    G.add_edge('c', 'f', weight=0.9)
    G.add_edge('a', 'd', weight=0.3)
    G.add_edge('g', 'd', weight=0.5)
    G.add_edge('h', 'f', weight=0.2)
    G.add_edge('g', 'f', weight=0.3)
    return G

def format_and_display_result(results):
    pos = nx.spring_layout(G)
    labels = nx.get_edge_attributes(G, 'weight')
    nx.draw_networkx_nodes(G, pos, node_size=700)
    nx.draw_networkx_edges(G, pos, width=6, label=results.values())
    nx.draw_networkx_edge_labels(G, pos, edge_labels=labels)
    nx.draw_networkx_labels(G, pos, labels=results, font_size=10, font_family='sans-serif')
    plt.show()



if __name__ == "__main__":
    G = compose_graph()
    print "This program will calculate shortest path from source in a graph to every other node"
    print "These are available nodes:"
    print G.nodes
    print "\n"
    source = raw_input("Which node do you want as a source? ")
    try:
        results, previous = dijkstra_algorithm_simple(G,source)
    except AttributeError:
        print "There is no node called %s " % source
        print "Please choose a valid node"
        sys.exit(1)
    print "Results:"
    print "Cost of paths to nodes: ", results
    print "Previous nodes on shortest path: ", previous
    format_and_display_result(results)

